import './js/navbar.js';

import './scss/abstracts/colors.scss';

import './scss/typography.scss';

import './scss/abstracts/animations.scss';
import './scss/abstracts/breackpoints.scss';

import './scss/reset.scss';
import './scss/components/header.scss';
import './scss/components/footer.scss';
