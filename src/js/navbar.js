$(document).ready(function() {
  var flag = false;
  $(".dropdown-toggler").on('mouseover', function() {
      $(".nav-services-list").show().addClass('fadeIn');
  });
  $(".dropdown-toggler").on('mouseleave', function() {
      if (!flag) {
        $(".nav-services-list").hide().removeClass('fadeIn');
      }

  });
  $(".nav-services-list").on('mouseover', function() {
    $(".nav-services-list").show().addClass('fadeIn');
    flag = true;
  });
  $(".nav-services-list").on('mouseleave', function() {
      $(".nav-services-list").hide().removeClass('fadeIn');
    flag = false;
  });
});
